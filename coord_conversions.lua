-- New coordinate conversion functions:
-- World: global geometric coordinates
-- Block: global block index coordinates
-- Chunk: chunk index coordinates and block-in-chunk coordinates
function GlobalToBlock(xg, yg, zg, bs)
    return math.floor(xg / bs + 0.5), math.floor(yg / bs + 0.5),
           math.floor(zg / bs + 0.5)
end
function BlockToGlobal(xb, yb, zb, bs) return xb * bs, yb * bs, zb * bs end
function BlockToChunk(xb, yb, zb, xpc, ypc, zpc)
    local Ci, Cj, Ck = math.floor((xb - 1) / xpc), math.floor((yb - 1) / ypc),
                       math.floor((zb - 1) / zpc)
    local xc, yc, zc = (xb - 1) % xpc + 1, (yb - 1) % ypc + 1,
                       (zb - 1) % zpc + 1
    return Ci, Cj, Ck, xc, yc, zc
end
function ChunkToBlock(Ci, Cj, Ck, xc, yc, zc, xpc, ypc, zpc)
    return Ci * xpc + xc, Cj * ypc + yc, Ck * zpc + zc
end
function GlobalToChunk(xg, yg, zg, bs, xpc, ypc, zpc)
    local x, y, z = GlobalToBlock(xg, yg, zg, bs)
    return BlockToChunk(x, y, z, xpc, ypc, zpc)
end
function ChunkToGlobal(Ci, Cj, Ck, xc, yc, zc, bs, xpc, ypc, zpc)
    local x, y, z = ChunkToBlock(Ci, Cj, Ck, xc, yc, zc, xpc, ypc, zpc)
    return BlockToGlobal(x, y, z, bs)
end
