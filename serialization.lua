local binser = require "external.binser.binser"
local bitser = require "external.bitser.bitser"
local serpent = require "external.serpent.serpent"
local smallfolk = require "external.smallfolk.smallfolk"
local ser = require "external.ser.ser"
local msgpack = require "external.messagepack.MessagePack"

local s = {}
s.method = "smallfolk"

function s:serialize(data)
    local sdata
    if s.method == "serpent" then
        sdata = serpent.dump(data, {maxlevel = 4})
    elseif s.method == "bitser" then
        sdata = bitser.dumps(data)
    elseif s.method == "binser" then
        sdata = binser.serialize(data)
    elseif s.method == "smallfolk" then
        sdata = smallfolk.dumps(data)
    elseif s.method == "ser" then
        sdata = ser(data)
    elseif s.method == "msgpack" then
        sdata = msgpack.pack(data)
    elseif s.method == "none" then
        sdata = data
    else
        error("Bad method")
    end
    return sdata
end

function s:deserialize(sdata)
    local data
    if s.method == "serpent" then
        _, data = serpent.load(sdata, {maxlevel = 4})
    elseif s.method == "bitser" then
        data = bitser.loads(sdata)
    elseif s.method == "binser" then
        data, _ = binser.deserialize(sdata)[1][1]
    elseif s.method == "smallfolk" then
        data = smallfolk.loads(sdata, 1000000)
    elseif s.method == "ser" then
        data = setfenv(loadstring(sdata), {})()
    elseif s.method == "msgpack" then
        data = msgpack.unpack(sdata)
    elseif s.method == "none" then
        data = sdata
    else
        error("Bad method")
    end
    return data
end

return s
