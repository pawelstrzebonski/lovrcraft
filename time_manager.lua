local TimeManager = {}
TimeManager.targetTime = 1 / 60
TimeManager.t0Update = 0.0
TimeManager.t0 = 0.0
TimeManager.meshingTime = 0.0
TimeManager.meshQueueTime = 0.0
TimeManager.collidingTime = 0.0
TimeManager.colliderQueueTime = 0.0
TimeManager.alpha = 0.5

local getTime = lovr.timer.getTime

function TimeManager:setTargetFPS(fps)
    -- TODO: Assumes target time is 2/3 of an update time (assumes 1/3 of time spent on rendering, estimate rendering time to adjust this?)
    TimeManager.targetTime = 0.66 / fps
end

function TimeManager:startUpdateTime() TimeManager.t0Update = getTime() end

function TimeManager:startTime() TimeManager.t0 = getTime() end

function TimeManager:stopMeshingTime()
    local dt = getTime() - TimeManager.t0
    TimeManager.meshingTime = TimeManager.alpha * dt + (1 - TimeManager.alpha) *
                                  TimeManager.meshingTime
end

function TimeManager:stopMeshQueuingTime()
    local dt = getTime() - TimeManager.t0
    TimeManager.meshQueueTime =
        TimeManager.alpha * dt + (1 - TimeManager.alpha) *
            TimeManager.meshQueueTime
end

function TimeManager:stopCollidingTime()
    local dt = getTime() - TimeManager.t0
    TimeManager.collidingTime =
        TimeManager.alpha * dt + (1 - TimeManager.alpha) *
            TimeManager.collidingTime
end

function TimeManager:stopColliderQueuingTime()
    local dt = getTime() - TimeManager.t0
    TimeManager.colliderQueueTime = TimeManager.alpha * dt +
                                        (1 - TimeManager.alpha) *
                                        TimeManager.colliderQueueTime
end

function TimeManager:shouldIMesh()
    return TimeManager.targetTime - (getTime() - TimeManager.t0Update) >
               TimeManager.meshingTime
end

function TimeManager:shouldIQueueMesh()
    return TimeManager.targetTime - (getTime() - TimeManager.t0Update) >
               TimeManager.meshQueueTime
end

function TimeManager:shouldIQueueCollider()
    return TimeManager.targetTime - (getTime() - TimeManager.t0Update) >
               TimeManager.colliderQueueTime
end

function TimeManager:shouldICollide()
    return TimeManager.targetTime - (getTime() - TimeManager.t0Update) >
               TimeManager.collidingTime
end

return TimeManager
