local lovr = {filesystem = require "lovr.filesystem"}
local VoxelConf = {}

local TOML = require "external.lua-toml.toml"

function VoxelConf:setupConfig(filename)
    local contents, bytes = lovr.filesystem.read(filename, -1)
    vc = TOML.parse(contents)
    VoxelConf.XperChunk = vc.XperChunk or 16
    VoxelConf.YperChunk = vc.YperChunk or VoxelConf.XperChunk
    VoxelConf.ZperChunk = vc.ZperChunk or VoxelConf.XperChunk
    VoxelConf.ChunksHeight = vc.ChunksHeight or 24
    VoxelConf.BlockSize = vc.BlockSize or 1 / 4
    VoxelConf.printFPS = vc.printFPS or false
    VoxelConf.loadRange = vc.loadRange or 3
    VoxelConf.unloadRange = vc.unloadRange or VoxelConf.loadRange + 1
    VoxelConf.culling = vc.culling or true
    VoxelConf.meshinglevel = vc.meshinglevel or 3
    VoxelConf.targetFPS = vc.targetFPS or 75
    VoxelConf.cullSides = vc.cullSides or false
end

return VoxelConf
