local lovr = {
    thread = require 'lovr.thread',
    math = require 'lovr.math',
    filesystem = require 'lovr.filesystem',
    timer = require 'lovr.timer'
}
local perlin = require "external.perlin.perlin"
local atlasconf = require "atlastexture"
local serializer = require("serialization")
require "coord_conversions"

local mesher2world = lovr.thread.getChannel('mesher2world')
local world2mesher = lovr.thread.getChannel('world2mesher')
local collider2world = lovr.thread.getChannel('collider2world')
local world2collider = lovr.thread.getChannel('world2collider')
local main2world = lovr.thread.getChannel('main2world')
local world2main = lovr.thread.getChannel('world2main')

local blockDict = {}
local lightDict = {}
local minY = 1
local maxY = nil
local minid, maxid = math.huge, 0
local maxLight = 4.0
local VoxelConf = require "voxelconf"
local blocklookup

function setup(atlasdecoderfile, vc)
    VoxelConf:setupConfig(vc)
    local _, _, blu, _ = atlasconf:make_atlas_decoder(atlasdecoderfile)
    blocklookup = blu
    for k, v in pairs(blocklookup) do
        if v < minid then minid = v end
        if v > maxid then maxid = v end
    end
    perlin:load()
    maxY = VoxelConf.YperChunk * VoxelConf.ChunksHeight
end

local function getWorldBlock(x, y, z)
    local N = 100
    -- Calculate block type
    local t = 0
    -- Calculate surface height using perlin noise
    local surface = 130 + 60 * perlin:noise(x / N, z / N, 0)
    local caveheight = 70 + 120 * perlin:noise(x / (2 * N), z / (2 * N), 0)
    local iscave = perlin:noise(x / (3 * N), z / (3 * N), 0) < 0.5
    if iscave and math.abs(caveheight - y) < 5 then return 0 end
    local stone = blocklookup["stone"]
    local grass = blocklookup["grass"]
    if y <= surface then
        if y <= 130 then
            t = stone
        else
            t = grass
        end
    end
    return t
end

function getBlock(x, y, z)
    if blockDict[x] == nil then blockDict[x] = {} end
    if blockDict[x][y] == nil then blockDict[x][y] = {} end
    if blockDict[x][y][z] == nil then
        blockDict[x][y][z] = getWorldBlock(x, y, z)
    end
    return blockDict[x][y][z]
end

function getBlocks(xmin, ymin, zmin, xmax, ymax, zmax)
    local blocks = {}
    for i = 1, xmax - xmin + 1 do
        blocks[i] = {}
        for j = 1, ymax - ymin + 1 do
            blocks[i][j] = {}
            for k = 1, zmax - zmin + 1 do
                blocks[i][j][k] = getBlock(i + xmin - 1, j + ymin - 1,
                                           k + zmin - 1)
            end
        end
    end
    return blocks
end

function getChunkBlocks(Cx, Cy, Cz)
    return getBlocks(Cx * VoxelConf.XperChunk + 1, Cy * VoxelConf.YperChunk + 1,
                     Cz * VoxelConf.ZperChunk + 1,
                     (Cx + 1) * VoxelConf.XperChunk,
                     (Cy + 1) * VoxelConf.YperChunk,
                     (Cz + 1) * VoxelConf.ZperChunk)
end

function getChunkBlocksPadded(Cx, Cy, Cz)
    return getBlocks(Cx * VoxelConf.XperChunk, Cy * VoxelConf.YperChunk,
                     Cz * VoxelConf.ZperChunk,
                     (Cx + 1) * VoxelConf.XperChunk + 1,
                     (Cy + 1) * VoxelConf.YperChunk + 1,
                     (Cz + 1) * VoxelConf.ZperChunk + 1)
end

function removeBlock(X, Y, Z)
    local b = getBlock(X, Y, Z)
    blockDict[X][Y][Z] = 0
    local chunkX, chunkY, chunkZ, x, y, z =
        BlockToChunk(X, Y, Z, VoxelConf.XperChunk, VoxelConf.YperChunk,
                     VoxelConf.ZperChunk)
    local chunksToUpdate = {{chunkX, chunkY, chunkZ}}
    if x == 1 then
        chunksToUpdate[#chunksToUpdate + 1] = {chunkX - 1, chunkY, chunkZ}
    elseif x == VoxelConf.XperChunk then
        chunksToUpdate[#chunksToUpdate + 1] = {chunkX + 1, chunkY, chunkZ}
    end
    if y == 1 then
        chunksToUpdate[#chunksToUpdate + 1] = {chunkX, chunkY - 1, chunkZ}
    elseif y == VoxelConf.YperChunk then
        chunksToUpdate[#chunksToUpdate + 1] = {chunkX, chunkY + 1, chunkZ}
    end
    if z == 1 then
        chunksToUpdate[#chunksToUpdate + 1] = {chunkX, chunkY, chunkZ - 1}
    elseif z == VoxelConf.ZperChunk then
        chunksToUpdate[#chunksToUpdate + 1] = {chunkX, chunkY, chunkZ + 1}
    end
    return b, chunksToUpdate
end

function addBlock(X, Y, Z, b)
    local b0 = getBlock(X, Y, Z)
    blockDict[X][Y][Z] = b
    local cx, cy, cz, _, _, _ = BlockToChunk(X, Y, Z, VoxelConf.XperChunk,
                                             VoxelConf.YperChunk,
                                             VoxelConf.ZperChunk)
    return {{cx, cy, cz}}
end

function getLight(x, y, z)
    if lightDict[x] == nil then lightDict[x] = {} end
    if lightDict[x][y] == nil then lightDict[x][y] = {} end
    if lightDict[x][y][z] == nil then lightDict[x][y][z] = 0.0 end
    local l = lightDict[x][y][z]
    if l < 0 then
        l = 0.0
    elseif l > 1 then
        l = 1.0
    end
    return l
end

function getLights(xmin, ymin, zmin, xmax, ymax, zmax)
    local blocks = {}
    for i = 1, xmax - xmin + 1 do
        blocks[i] = {}
        for j = 1, ymax - ymin + 1 do
            blocks[i][j] = {}
            for k = 1, zmax - zmin + 1 do
                blocks[i][j][k] = getLight(i + xmin - 1, j + ymin - 1,
                                           k + zmin - 1)
            end
        end
    end
    return blocks
end

function getChunkLights(Cx, Cy, Cz)
    return getLights(Cx * VoxelConf.XperChunk + 1, Cy * VoxelConf.YperChunk + 1,
                     Cz * VoxelConf.ZperChunk + 1,
                     (Cx + 1) * VoxelConf.XperChunk + 1,
                     (Cy + 1) * VoxelConf.YperChunk + 1,
                     (Cz + 1) * VoxelConf.ZperChunk + 1)
end

function getChunkLightsPadded(Cx, Cy, Cz)
    return getLights(Cx * VoxelConf.XperChunk, Cy * VoxelConf.YperChunk,
                     Cz * VoxelConf.ZperChunk,
                     (Cx + 1) * VoxelConf.XperChunk + 2,
                     (Cy + 1) * VoxelConf.YperChunk + 2,
                     (Cz + 1) * VoxelConf.ZperChunk + 2)
end
function setLight(x, y, z, L)
    if lightDict[x] == nil then lightDict[x] = {} end
    if lightDict[x][y] == nil then lightDict[x][y] = {} end
    lightDict[x][y][z] = L
end
local function illuminateNeighbors(x, y, z, L)
    if getBlock(x, y, z) == 0 then
        if getLight(x, y, z) * maxLight < L then
            setLight(x, y, z, L / maxLight)
        end
        if L > 1 then
            illuminateNeighbors(x - 1, y, z, (L - 1))
            illuminateNeighbors(x + 1, y, z, (L - 1))
            illuminateNeighbors(x, y - 1, z, (L - 1))
            illuminateNeighbors(x, y + 1, z, (L - 1))
            illuminateNeighbors(x, y, z - 1, (L - 1))
            illuminateNeighbors(x, y, z + 1, (L - 1))
        end
    end
end
function shineSkyLight(x, z, relight)
    if not relight and getLight(x, maxY, z) > 0 then return end
    for y = maxY, minY, -1 do
        if getBlock(x, y, z) == 0 then
            setLight(x, y, z, maxLight)
        else
            illuminateNeighbors(x, y + 1, z, maxLight)
            break
        end
    end
end

function mkrng(a, b)
    if a > b then
        return b, a
    elseif a < b then
        return a, b
    else
        return a, a
    end
end
function planeLineIntersect(lp, lv, pp, pv)
    local t = -pv:dot(lp - pp) / lv:dot(pv)
    return t
end
-- TODO grid-based raycaster
function raycast(p0, v0)
    local start, finish = p0, p0 + v0
    local intersections = {}
    local a, b = mkrng(start.x, finish.x)
    -- TODO: increment by block size, get correct offset
    for xp = a, b do
        local t = planeLineIntersect(p0, v0, vec3(xp, 0, 0), vec3(1, 0, 0))
        if 0 < t and t < 1 then intersections[#intersections + 1] = t end
    end
    local a, b = mkrng(start.y, finish.y)
    for yp = a, b do
        local t = planeLineIntersect(p0, v0, vec3(0, yp, 0), vec3(0, 1, 0))
        if 0 < t and t < 1 then intersections[#intersections + 1] = t end
    end
    local a, b = mkrng(start.z, finish.z)
    for zp = a, b do
        local t = planeLineIntersect(p0, v0, vec3(0, 0, zp), vec3(0, 0, 1))
        if 0 < t and t < 1 then
            intersections[#intersections + 1] = p0 + t
        end
    end
    -- TODO: sort intersections
    -- TODO: convert intersections to blocks to-query
    -- TODO: find first intersection with block by querying block data
end

local function process(input, oc)
    local input = serializer:deserialize(input)
    if input.fun == "setup" then
        setup(input.atlasdecoderfile, input.vc)
        return
    elseif input.fun == "getChunkBlocks" then
        -- print("Recieved block order", input.Cx, input.Cy, input.Cz)
        input.blocks = getChunkBlocks(input.Cx, input.Cy, input.Cz)
    elseif input.fun == "getChunkBlocksPadded" then
        -- print("Recieved block order", input.Cx, input.Cy, input.Cz)
        input.blocks = getChunkBlocksPadded(input.Cx, input.Cy, input.Cz)
    elseif input.fun == "getChunkLights" then
        -- print("Recieved light order", input.Cx, input.Cy, input.Cz)
        input.light = getChunkLights(input.Cx, input.Cy, input.Cz)
    elseif input.fun == "getChunkLightsPadded" then
        -- print("Recieved light order", input.Cx, input.Cy, input.Cz)
        input.light = getChunkLightsPadded(input.Cx, input.Cy, input.Cz)
    elseif input.fun == "addBlock" then
        input.chunk = addBlock(input.X, input.Y, input.Z, input.b)
    elseif input.fun == "removeBlock" then
        input.b, input.chunk = removeBlock(input.X, input.Y, input.Z)
    end
    local dat = serializer:serialize(input)
    oc:push(dat)
    -- print("Pushed block data", input.Cx, input.Cy, input.Cz)
end

while true do
    while true do
        input = main2world:pop()
        if input ~= nil then
            process(input, world2main)
        else
            break
        end
    end
    input = mesher2world:pop()
    if input ~= nil then process(input, world2mesher) end
    input = collider2world:pop()
    if input ~= nil then process(input, world2collider) end
    lovr.timer.sleep(1 / 120)
end
