-- Print Lua version for reference
print(_VERSION)

local ChunkMeshManager = require("chunk_graphics")
local ChunkColliderManager = require("chunk_physics")
local VoxelConf = require "voxelconf"
local atlasconf = require "atlastexture"
local motion = require "motion"
local controller = require("controller")
local serializer = require("serialization")
local TimeManager = require("time_manager")
require "coord_conversions"

local main2world = lovr.thread.getChannel('main2world')
local world2main = lovr.thread.getChannel('world2main')

local printTimer = 0.0

-- require "coord_test"
-- lovr.event.quit()

local shader

-- Block data information
local blocklookup
local minid, maxid = math.huge, 0

function lovr.load()
    -- Load Voxel configuration
    VoxelConf:setupConfig("config/blocks.toml")
    -- Load block data
    ChunkMeshManager:setup("derived/atlas.png", VoxelConf)
    ChunkColliderManager:setup(VoxelConf)
    motion:setup(VoxelConf)
    controller:setup(VoxelConf)

    TimeManager:setTargetFPS(VoxelConf.targetFPS)

    -- Make table of update chunk coordinates
    ChunkMeshManager:update_range(VoxelConf.loadRange)

    -- Load a skybox
    skybox = lovr.graphics.newTexture({
        left = "derived/clouds1_west.jpg",
        right = "derived/clouds1_east.jpg",
        top = "derived/clouds1_up.jpg",
        bottom = "derived/clouds1_down.jpg",
        front = "derived/clouds1_south.jpg",
        back = "derived/clouds1_north.jpg"
    })

    -- Create a new thread called 'thread' using the code above
    local threadCode, _ = lovr.filesystem.read("chunk_mesher.lua", -1)
    -- Start the thread
    local chunkerThreads = {}
    for i = 1, 1 do
        chunkerThreads[i] = lovr.thread.newThread(threadCode)
        chunkerThreads[i]:start()
    end
    local threadCode, _ = lovr.filesystem.read("chunk_collider.lua", -1)
    local colliderThreads = {}
    for i = 1, 1 do
        colliderThreads[i] = lovr.thread.newThread(threadCode)
        colliderThreads[i]:start()
    end
    local threadCode, _ = lovr.filesystem.read("world_data_server.lua", -1)
    worldThread = lovr.thread.newThread(threadCode)
    worldThread:start()
    -- Setup block data
    main2world:push(serializer:serialize({
        fun = "setup",
        atlasdecoderfile = "derived/atlasdecoder.toml",
        vc = "config/blocks.toml"
    }))
    -- Load block data here as well
    local _, _, blu, _ = atlasconf:make_atlas_decoder(
                             "derived/atlasdecoder.toml")
    blocklookup = blu
    for k, v in pairs(blocklookup) do
        if v < minid then minid = v end
        if v > maxid then maxid = v end
    end

    defaultVertex = lovr.graphics.compileShader('vertex', [[
        vec4 lovrmain()
    {
        return Projection * View * Transform * VertexPosition;
    }
    ]])
    defaultFragment = lovr.graphics.compileShader('fragment', [[
        Constants {
      vec4 ambience;
      vec4 lightColor;
      vec3 lightPos;
      float specularStrength;
      int metallic;
    };

    vec4 lovrmain()
    {
        //diffuse
        vec3 norm = normalize(Normal);
        vec3 lightDir = normalize(lightPos - PositionWorld);
        float diff = max(dot(norm, lightDir), 0.0);
        vec4 diffuse = diff * lightColor;

        //specular
        vec3 viewDir = normalize(CameraPositionWorld - PositionWorld);
        vec3 reflectDir = reflect(-lightDir, norm);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), metallic);
        vec4 specular = specularStrength * spec * lightColor;

        vec4 baseColor = Color * getPixel(ColorTexture, UV);
        return baseColor * (ambience + diffuse + specular);
    }
    ]])
    shader = lovr.graphics.newShader(defaultVertex, defaultFragment, {})
end

function lovr.update(dt)
    TimeManager:startUpdateTime()

    -- Handle commands from world server
    while true do
        local input = world2main:pop()
        if input ~= nil then
            input = serializer:deserialize(input)
            for i = 1, #input.chunk do
                local C = input.chunk[i]
                ChunkMeshManager:updateChunk(C[1], C[2], C[3], true)
                ChunkColliderManager:updateChunk(C[1], C[2], C[3], true)
            end
            -- TODO: handle returned mined block
        else
            break
        end
    end

    local newbox = controller:update(ChunkMeshManager, motion)
    if newbox ~= nil then
        local C = WorldData:addBlock(newbox[1], newbox[2], newbox[3], 1)
        print(C[1], C[2], C[3])
        ChunkMeshManager:updateChunk(C[1], C[2], C[3])
        ChunkColliderManager:updateChunk(C[1], C[2], C[3])
    end
    local pos = mat4(motion.pose):mul(mat4(lovr.headset.getViewPose(1))):mul(
                    vec3())
    local chunkX, chunkY, chunkZ, x, y, z =
        GlobalToChunk(pos.x, pos.y, pos.z, VoxelConf.BlockSize,
                      VoxelConf.XperChunk, VoxelConf.YperChunk,
                      VoxelConf.ZperChunk)
    ChunkMeshManager:update(dt, chunkX, chunkY, chunkZ, TimeManager)
    ChunkColliderManager:update(dt, chunkX, chunkY, chunkZ, TimeManager)
    motion:update(dt)
    printTimer = printTimer + dt
end

local blockid = 1
local addblock = true

function lovr.draw(pass)
    -- TODO: set "WrapMode" as "repeat" to get tiled textures
    pass:setWinding('counterclockwise')
    pass:setCullMode('back')
    -- pass:setBlendMode('alpha')
    pass:setSampler('nearest')

    pass:skybox(skybox)

    motion:draw(pass)

    -- Set default shader values
    -- pass:setShader(shader)
    -- pass:send('lightColor', {1.0, 1.0, 1.0, 1.0})
    -- pass:send('ambience', {0.1, 0.1, 0.1, 1.0})
    -- pass:send('lightPos', {100, 200, 100})

    local x, y, z, angle, ax, ay, az = lovr.headset.getViewPose(1)
    local pos = mat4(motion.pose):mul(mat4(lovr.headset.getViewPose(1))):mul(
                    vec3())
    local viewvec = lovr.math.quat(angle, ax, ay, az):direction()
    local t0 = lovr.timer.getTime()
    local rendered = ChunkMeshManager:draw(pass, pos, viewvec,
                                           VoxelConf.cullSides)
    -- Print the FPS
    if VoxelConf.printFPS and printTimer >= 1 then
        print(lovr.timer.getFPS() .. " FPS rendering " .. rendered .. " faces")
        print("TimeManager.meshingTime", "TimeManager.meshQueueTime",
              "TimeManager.collidingTime", "TimeManager.colliderQueueTime")
        print(TimeManager.meshingTime, TimeManager.meshQueueTime,
              TimeManager.collidingTime, TimeManager.colliderQueueTime)
        -- stats = lovr.graphics.getStats()
        -- for k, v in pairs(stats) do print(k, "=>", v) end
        printTimer = 0
    end
    pass:text(lovr.timer.getFPS() .. " FPS rendering " .. rendered .. " faces",
              pos.x + 10 * viewvec.x, pos.y + 10 * viewvec.y,
              pos.z + 10 * viewvec.z, 1, angle, ax, ay, az)
end

function lovr.keypressed(key, scancode, repeating)
    -- Press ESC to quit
    if key == "escape" then
        lovr.event.quit()
    elseif key == "p" then
        VoxelConf.printFPS = not VoxelConf.printFPS
    elseif key == "r" then
        addblock = not addblock
    elseif key == "o" then
        lovr.graphics.setWireframe(not lovr.graphics.isWireframe())
    elseif key == "-" then
        VoxelConf.loadRange = math.max(1, VoxelConf.loadRange - 1)
        VoxelConf.unloadRange = math.max(2, VoxelConf.unloadRange - 1)
        ChunkMeshManager:update_range(VoxelConf.loadRange)
    elseif key == "=" then
        VoxelConf.loadRange = VoxelConf.loadRange + 1
        VoxelConf.unloadRange = VoxelConf.unloadRange + 1
        ChunkMeshManager:update_range(VoxelConf.loadRange)
    elseif key == "c" then
        VoxelConf.cullSides = not VoxelConf.cullSides
    elseif key == "[" and blockid > minid then
        blockid = blockid - 1
    elseif key == "]" and blockid < maxid then
        blockid = blockid + 1
    elseif key == "f" then
        local x, y, z, angle, ax, ay, az = lovr.headset.getViewPose(1)
        local pos = mat4(motion.pose):mul(mat4(lovr.headset.getViewPose(1)))
                        :mul(vec3())
        local vec = lovr.math.quat(angle, ax, ay, az):direction()
        -- TODO: reimplement, add checking of world2main and handling pushed chunk updates
        local blockcoord, spacecoord = ChunkColliderManager:raycast(pos,
                                                                    vec * 16)
        print(blockcoord, spacecoord)
        if blockcoord ~= nil then
            if addblock then
                reqdata = {
                    X = spacecoord[1],
                    Y = spacecoord[2],
                    Z = spacecoord[3],
                    b = blockid,
                    fun = "addBlock"
                }
                local req = serializer:serialize(reqdata)
                main2world:push(req)
                --[[
                local C = WorldData:addBlock(spacecoord[1], spacecoord[2],
                                             spacecoord[3], blockid)
                print("To-Update: ", vec3(C[1], C[2], C[3]))
                ChunkMeshManager:updateChunk(C[1], C[2], C[3])
                ChunkColliderManager:updateChunk(C[1], C[2], C[3])
                --]]
            else
                reqdata = {
                    X = spacecoord[1],
                    Y = spacecoord[2],
                    Z = spacecoord[3],
                    fun = "removeBlock"
                }
                local req = serializer:serialize(reqdata)
                main2world:push(req)
                --[[
                local b, Cs = WorldData:removeBlock(blockcoord[1],
                                                    blockcoord[2], blockcoord[3])
                for i = 1, #Cs do
                    local C = Cs[i]
                    print("To-Update: ", vec3(C[1], C[2], C[3]))
                    ChunkMeshManager:updateChunk(C[1], C[2], C[3])
                    ChunkColliderManager:updateChunk(C[1], C[2], C[3])
                end
                --]]
            end
        end
    end
end
