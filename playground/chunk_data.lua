local Serialize = require 'knife.serialize'
local binser = require "binser"

local data = {}
for i = 1, 16 do
    data[i] = {}
    for j = 1, 16 do
        data[i][j] = {}
        for k = 1, 16 * 16 do data[i][j][k] = i + j + k end
    end
end

local savegame = Serialize(data)
local file = io.open("test.save", "w")
file:write(savegame)
file:close()

local binary_data = binser.serialize(data)
local file = io.open("test.msgpack", "w")
file:write(binary_data)
file:close()
