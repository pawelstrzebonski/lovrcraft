local VoxelConf = require "voxelconf"
VoxelConf:setupConfig("config/blocks.toml")
local WorldData = require "world_data"
WorldData:setup("derived/atlasdecoder.toml", VoxelConf)

for i = -100, 100 do
    local j, k = 0, 0
    local a, b, c = WorldData:GlobalToBlock(i, j, k)
    local i2, _, _ = WorldData:BlockToGlobal(a, b, c)
    if i ~= i2 then print("Error: " .. i .. "=>" .. i2 .. " via " .. a) end
end
print("Passed Global<=>Block")

for i = -100, 100 do
    local j, k = 0, 0
    local a, b, c, d, e, f = WorldData:BlockToChunk(i, j, k)
    local i2, _, _ = WorldData:ChunkToBlock(a, b, c, d, e, f)
    if i ~= i2 then
        print("Error: " .. i .. "=>" .. i2 .. " via " .. a .. "," .. d)
    end
end
print("Passed Chunk<=>Block")

for i = -100, 100 do
    local j, k = 0, 0
    local a, b, c, d, e, f = WorldData:GlobalToChunk(i, j, k)
    local i2, _, _ = WorldData:ChunkToGlobal(a, b, c, d, e, f)
    if i ~= i2 then
        print("Error: " .. i .. "=>" .. i2 .. " via " .. a .. "," .. d)
    end
end
print("Passed Chunk<=>Global")

