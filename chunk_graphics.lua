local serializer = require("serialization")
local atlasconf = require("atlastexture")
require "coord_conversions"

local outchannel = lovr.thread.getChannel("chunkmeshrequests")
local outchannelpriority = lovr.thread.getChannel("prioritychunkmeshrequests")
local inchannel = lovr.thread.getChannel("chunkmeshresults")

local ChunkManager = {}
ChunkManager.chunkDict = {}
ChunkManager.renderList = {}
ChunkManager.updateList = {}
ChunkManager.materialAtlas = nil
ChunkManager.sidelookup = nil

local VoxelConf

function ChunkManager:setup(atlasfile, vc)
    local _, sidelookup, _, _ = atlasconf:make_atlas_decoder(
                                    "derived/atlasdecoder.toml")
    ChunkManager.sidelookup = sidelookup
    local t = lovr.graphics.newTexture(atlasfile)
    ChunkManager.materialAtlas = t -- lovr.graphics.newMaterial(t)
    VoxelConf = vc
end

function ChunkManager:newChunk(Cx, Cy, Cz, isupdate)
    local isupdate = isupdate or false
    local xmin, _, zmin = ChunkToBlock(Cx, Cy, Cz, 1, 1, 1, VoxelConf.XperChunk,
                                       VoxelConf.YperChunk, VoxelConf.ZperChunk)
    local xmax, _, zmax = ChunkToBlock(Cx, Cy, Cz, VoxelConf.XperChunk, 1,
                                       VoxelConf.ZperChunk, VoxelConf.XperChunk,
                                       VoxelConf.YperChunk, VoxelConf.ZperChunk)
    local self = {}
    self.center = lovr.math.newVec3(ChunkToGlobal(Cx, Cy, Cz,
                                                  VoxelConf.XperChunk / 2,
                                                  VoxelConf.YperChunk / 2,
                                                  VoxelConf.ZperChunk / 2,
                                                  VoxelConf.BlockSize,
                                                  VoxelConf.XperChunk,
                                                  VoxelConf.YperChunk,
                                                  VoxelConf.ZperChunk))
    local offset = lovr.math.newVec3(vec3(
                                         ChunkToGlobal(Cx, Cy, Cz, 1, 1, 1,
                                                       VoxelConf.BlockSize,
                                                       VoxelConf.XperChunk,
                                                       VoxelConf.YperChunk,
                                                       VoxelConf.ZperChunk)) -
                                         VoxelConf.BlockSize / 2)
    local a, b = self.center -
                     vec3(VoxelConf.XperChunk, VoxelConf.YperChunk,
                          VoxelConf.ZperChunk) * VoxelConf.BlockSize / 2,
                 self.center +
                     vec3(VoxelConf.XperChunk, VoxelConf.YperChunk,
                          VoxelConf.ZperChunk) * VoxelConf.BlockSize / 2
    self.xmin, self.xmax, self.ymin, self.ymax, self.zmin, self.zmax = a[1],
                                                                       b[1],
                                                                       a[2],
                                                                       b[2],
                                                                       a[3],
                                                                       b[3]
    self.corners = {
        lovr.math.newVec3(self.xmin, self.ymin, self.zmin),
        lovr.math.newVec3(self.xmin, self.ymin, self.zmax),
        lovr.math.newVec3(self.xmin, self.ymax, self.zmin),
        lovr.math.newVec3(self.xmin, self.ymax, self.zmax),
        lovr.math.newVec3(self.xmax, self.ymin, self.zmin),
        lovr.math.newVec3(self.xmax, self.ymin, self.zmax),
        lovr.math.newVec3(self.xmax, self.ymax, self.zmin),
        lovr.math.newVec3(self.xmax, self.ymax, self.zmax)
    }
    -- Send request for mesh
    local reqdata = {
        offset = lovr.math.newVec3(offset),
        Cx = Cx,
        Cy = Cy,
        Cz = Cz,
        scale = VoxelConf.BlockSize,
        xpc = VoxelConf.XperChunk,
        ypc = VoxelConf.YperChunk,
        zpc = VoxelConf.ZperChunk
    }
    reqdata.offset = {reqdata.offset[1], reqdata.offset[2], reqdata.offset[3]}
    local req = serializer:serialize(reqdata)
    if isupdate then
        outchannelpriority:push(req)
    else
        outchannel:push(req)
    end
    if ChunkManager.chunkDict[Cx] == nil then ChunkManager.chunkDict[Cx] = {} end
    if ChunkManager.chunkDict[Cx][Cy] == nil then
        ChunkManager.chunkDict[Cx][Cy] = {}
    end
    ChunkManager.chunkDict[Cx][Cy][Cz] = self
end

function ChunkManager:updateChunk(Cx, Cy, Cz)
    -- TODO: add option to newChunk to retain old mesh before updating it?
    if ChunkManager.chunkDict[Cx] ~= nil and ChunkManager.chunkDict[Cx][Cy] ~=
        nil and ChunkManager.chunkDict[Cx][Cy][Cz] ~= nil then
        ChunkManager.chunkDict[Cx][Cy][Cz] = nil
    end
    ChunkManager:newChunk(Cx, Cy, Cz, true)
end

local meshFormat = {
    {type = 'vec3', location = 'VertexPosition'},
    {type = 'vec3', location = 'VertexColor'},
    {type = 'vec2', location = 'VertexUV'},
    {type = 'vec4', location = 'VertexColor'}
}
local function addMeshToChunk(msg)
    local resp = serializer:deserialize(msg)
    local Cx, Cy, Cz = resp.Cx, resp.Cy, resp.Cz
    if ChunkManager.chunkDict[Cx] == nil or ChunkManager.chunkDict[Cx][Cy] ==
        nil or ChunkManager.chunkDict[Cx][Cy][Cz] == nil then
        print("NOTICE: Adding mesh to nil chunk")
        return
    end
    if resp.vertices == nil or resp.indices == nil then
        ChunkManager.chunkDict[Cx][Cy][Cz].isempty = true
        -- print("NIL RESPONSE: addMeshToChunk", Cx, Cy, Cz)
        return
    end
    -- print("REAL RESPONSE: addMeshToChunk", Cx, Cy, Cz, #resp.vertices)
    ChunkManager.chunkDict[Cx][Cy][Cz].isempty = false
    ChunkManager.chunkDict[Cx][Cy][Cz].vertexBuffer =
        lovr.graphics.newBuffer(resp.vertices, meshFormat)
    ChunkManager.chunkDict[Cx][Cy][Cz].indexBuffer = {}
    ChunkManager.chunkDict[Cx][Cy][Cz].indexRange = {}
    local indices = {}
    for side = 1, 6 do
        local istart = #indices + 1
        for i = 1, #resp.indices[side] do
            indices[#indices + 1] = resp.indices[side][i]
        end
        ChunkManager.chunkDict[Cx][Cy][Cz].indexRange[side] = {
            istart, #resp.indices[side]
        }
        ChunkManager.chunkDict[Cx][Cy][Cz].indexBuffer[side] = lovr.graphics
                                                                   .newBuffer(
                                                                   resp.indices[side],
                                                                   'index16')
    end
end

function drawChunk(self, pass, pos, viewvec, cullSides)
    if self.isempty then return 0 end
    if self.vertexBuffer == nil or self.indexBuffer == nil then
        pass:setColor(1, 0, 1, 1)
        t = lovr.math.mat4(lovr.math.vec3(
                               self.center[1] - VoxelConf.BlockSize / 2,
                               self.center[2] - VoxelConf.BlockSize / 2,
                               self.center[3] - VoxelConf.BlockSize / 2),
                           lovr.math.vec3() * VoxelConf.XperChunk *
                               VoxelConf.BlockSize, lovr.math.quat(0, 0, 1, 0))
        pass:cube(t, "fill")
        pass:setColor(1, 1, 1, 1)
        return 0
    end
    local isvisible = false
    for i = 1, #self.corners do
        if (viewvec:dot((self.corners[i] - pos):normalize()) > 0) then
            isvisible = true
            break
        end
    end
    local rendered = 0
    if isvisible then
        if cullSides then
            local top, bottom, north, south, east, west =
                ChunkManager.sidelookup["top"],
                ChunkManager.sidelookup["bottom"],
                ChunkManager.sidelookup["north"],
                ChunkManager.sidelookup["south"],
                ChunkManager.sidelookup["east"], ChunkManager.sidelookup["west"]
            if pos.y > self.ymin then
                pass:mesh(self.vertexBuffer, self.indexBuffer[top],
                          lovr.math.mat4(), 1, nil, 1, 0)
                rendered = rendered + 1
            end
            if pos.y < self.ymax then
                pass:mesh(self.vertexBuffer, self.indexBuffer[bottom],
                          lovr.math.mat4(), 1, nil, 1, 0)
                rendered = rendered + 1
            end
            if pos.x > self.xmin then
                pass:mesh(self.vertexBuffer, self.indexBuffer[west],
                          lovr.math.mat4(), 1, nil, 1, 0)
                rendered = rendered + 1
            end
            if pos.x < self.xmax then
                pass:mesh(self.vertexBuffer, self.indexBuffer[east],
                          lovr.math.mat4(), 1, nil, 1, 0)
                rendered = rendered + 1
            end
            if pos.z > self.zmin then
                pass:mesh(self.vertexBuffer, self.indexBuffer[south],
                          lovr.math.mat4(), 1, nil, 1, 0)
                rendered = rendered + 1
            end
            if pos.z < self.zmax then
                pass:mesh(self.vertexBuffer, self.indexBuffer[north],
                          lovr.math.mat4(), 1, nil, 1, 0)
                rendered = rendered + 1
            end
        else
            -- TODO: convert into single call?
            for side = 1, 6 do
                pass:mesh(self.vertexBuffer, self.indexBuffer[side],
                          lovr.math.mat4(), 1, nil, 1, 0)
                rendered = rendered + 1
            end
        end
    end
    return rendered
end

function containsTable(t, v)
    for i, x in ipairs(t) do
        local same = true
        for j, z in ipairs(x) do if x[j] ~= v[j] then same = false end end
        if same then return true end
    end
    return false
end

function ChunkManager:update(dt, Px, Py, Pz, TimeManager)
    local abs = math.abs
    -- Remove out-of-range chunks
    for i, idx in ipairs(ChunkManager.renderList) do
        -- Get coordinates of chunk-under-test
        local Cx, Cy, Cz = idx[1], idx[2], idx[3]
        -- Test if outside unload range
        if abs(Cx - Px) > VoxelConf.unloadRange or abs(Cy - Py) >
            VoxelConf.unloadRange or abs(Cz - Pz) > VoxelConf.unloadRange then
            -- Remove object from dictionary
            ChunkManager.chunkDict[Cx][Cy][Cz] = nil
            -- Remove from list of chunks to render
            table.remove(ChunkManager.renderList, i)
        end
    end
    -- Load chunks within render range
    for x, coord in ipairs(ChunkManager.updateList) do
        -- Get relative coordinates
        local i, j, k = coord[1], coord[2], coord[3]
        -- Limit to world height limits
        if (Py + j >= 1) and (Py + j <= VoxelConf.ChunksHeight) then
            -- Calculate coordinates of chunk-under-test
            local Cx, Cy, Cz = Px + i, Py + j, Pz + k
            -- Check if chunk within list of to-render chunks
            if not containsTable(self.renderList, {Cx, Cy, Cz}) then
                -- If not, create the chunk and add to list
                TimeManager:startTime()
                ChunkManager:newChunk(Cx, Cy, Cz, false)
                ChunkManager.renderList[#ChunkManager.renderList + 1] = {
                    Cx, Cy, Cz
                }
                TimeManager:stopMeshQueuingTime()
            end
        end
        if not TimeManager:shouldIQueueCollider() then break end
    end

    local msg = inchannel:pop()
    while msg ~= nil do
        TimeManager:startTime()
        addMeshToChunk(msg)
        TimeManager:stopMeshingTime()
        if not TimeManager:shouldIMesh() then break end
        msg = inchannel:pop()
    end
end

function ChunkManager:draw(pass, pos, viewvec, cullSides)
    pass:setMeshMode('triangles')
    pass:setMaterial(ChunkManager.materialAtlas)
    local numrendered = 0
    for i, chunkidx in ipairs(ChunkManager.renderList) do
        local Cx, Cy, Cz = chunkidx[1], chunkidx[2], chunkidx[3]
        numrendered = numrendered +
                          drawChunk(ChunkManager.chunkDict[Cx][Cy][Cz], pass,
                                    pos, viewvec, cullSides)
    end
    return numrendered
end

function ChunkManager:update_range(lr)
    local updatelist = {}
    for i = -lr, lr do
        for j = -lr, lr do
            for k = -lr, lr do
                if i * i + j * j + k * k <= lr * lr then
                    updatelist[#updatelist + 1] = {i, j, k}
                end
            end
        end
    end
    local cmp = function(a, b)
        local asum = (math.abs(a[1]) + math.abs(a[2]) + math.abs(a[3]))
        local bsum = (math.abs(b[1]) + math.abs(b[2]) + math.abs(b[3]))
        if asum < bsum then
            return true
        elseif asum > bsum then
            return false
        else
            return false
        end
    end
    table.sort(updatelist, cmp)
    ChunkManager.updateList = updatelist
end

return ChunkManager
