local lovr = {filesystem = require "lovr.filesystem"}
local TOML = require "external.lua-toml.toml"
local atlasconf = {}

function atlasconf:make_atlas_decoder(filename)
    local contents, bytes = lovr.filesystem.read(filename, -1)
    local data = TOML.parse(contents)
    return data["uv"], data["side2id"], data["block2id"], data["sideblock2id"]
end

return atlasconf
