#!sh

# Make atlas texture
nix-shell --run "python make_atlas.py"

# Convert BMP skybox into JPEG skybox
mogrify -format jpg -path ../derived/ ../external/clouds1/*.bmp
