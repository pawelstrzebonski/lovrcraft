#from skimage import util
import toml
import os
import math

def make_atlas(filename, ntiles=4, pathprefix=""):
	conf=toml.load(filename)
	side2idx={"top":0,"bottom":1,"east":2,"west":3,"north":4,"south":5}
	block2id={block:(i) for (i,block) in enumerate(conf)}
	images=[]
	atlas=[[0 for _ in range(len(conf))] for _ in range(len(side2idx))]
	for block in conf:
		files=conf[block]["files"]
		if len(files)==1:
			images.append(pathprefix+files[0])
			idx=len(images)
			atlas[side2idx["top"]][block2id[block]]=idx
			atlas[side2idx["bottom"]][block2id[block]]=idx
			atlas[side2idx["north"]][block2id[block]]=idx
			atlas[side2idx["south"]][block2id[block]]=idx
			atlas[side2idx["east"]][block2id[block]]=idx
			atlas[side2idx["west"]][block2id[block]]=idx
		else:
			for (f, sides) in zip(conf[block]["files"],conf[block]["sides"]):
				images.append(pathprefix+f)
				idx=len(images)
				for side in sides:
					atlas[side2idx[side]][block2id[block]]=idx
	nwidth=math.ceil(math.sqrt(len(images)))
	nheight=math.ceil(len(images)/nwidth)
	img2uv={"x":[0 for _ in range(len(images))],"y":[0 for _ in range(len(images))]}
	command="convert"
	imagecounter=0
	for r in range(nheight):
		print("row",r)
		rowcommand=" \( "
		for c in range(nwidth):
			print("column",c)
			if imagecounter<len(images):
				rowcommand+=(images[imagecounter]+" ")*ntiles
				img2uv["y"][imagecounter]=[1-(c+o/ntiles)/(nwidth) for o in range(0,ntiles+1)]
				img2uv["x"][imagecounter]=[(r+o/ntiles)/(nheight) for o in range(0,ntiles+1)]
			else:
				break
			imagecounter+=1
		rowcommand+=" -append \)"
		command+=rowcommand*ntiles
	command+=" +append ../derived/atlas.png"
		
	os.system(command)
	
	#TODO: sanity checking (same square file sizes, total texture size not too big)
	
	# Increment to fix zero/one based index disparity
	for side in side2idx:
		side2idx[side]+=1
	for block in block2id:
		block2id[block]+=1
	
	return {"uv":img2uv, "side2id":side2idx, "block2id":block2id, "sideblock2id":atlas}

a=make_atlas("../config/atlas.toml", pathprefix="../external/minetest-game-mods/textures/")
t=toml.dumps(a)
with open("../derived/atlasdecoder.toml","w") as f:
	f.write(t)
