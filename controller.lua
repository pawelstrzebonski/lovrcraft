local controller = {}
local VoxelConf
local addblock = true
local blockid = 1

function controller:setup(vc) VoxelConf = vc end

function controller:update(ChunkMeshManager, motion)
    if lovr.headset.wasPressed("right", "trigger") then
        --[[
            local x, y, z, angle, ax, ay, az = lovr.headset.getPose("right")
        local pos = mat4(motion.pose):mul(mat4(lovr.headset.getPose("right")))
                        :mul(vec3())
        local vec = lovr.math.quat(angle, ax, ay, az):direction()
        local blockcoord, spacecoord = ChunkColliderManager:raycast(pos,
                                                                    vec * 16,
                                                                    WorldData)
        print(blockcoord, spacecoord)
        if blockcoord ~= nil then
            if addblock then
                local C = WorldData:addBlock(spacecoord[1], spacecoord[2],
                                             spacecoord[3], blockid)
                -- print(vec3(x, y, z), vec3(C[1], C[2], C[3]))
                ChunkMeshManager:updateChunk(WorldData, C[1], C[2], C[3])
            else
                local b, Cs = WorldData:removeBlock(blockcoord[1],
                                                    blockcoord[2], blockcoord[3])
                for i = 1, #Cs do
                    local C = Cs[i]
                    print(vec3(C[1], C[2], C[3]))
                    ChunkMeshManager:updateChunk(WorldData, C[1], C[2], C[3])
                end
            end
        end
        --]]
    end
    if lovr.headset.wasPressed("right", "a") and blockid > WorldData.minid then
        blockid = blockid - 1
    end
    if lovr.headset.wasPressed("right", "b") and blockid < WorldData.maxid then
        blockid = blockid + 1
    end
    if lovr.headset.wasPressed("right", "grip") then addblock = not addblock end
    return nil
end

return controller
