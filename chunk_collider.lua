local lovr = {
    thread = require 'lovr.thread',
    math = require 'lovr.math',
    filesystem = require 'lovr.filesystem',
    timer = require 'lovr.timer'
}
local serializer = require("serialization")

local inchannel = lovr.thread.getChannel('chunkcolliderrequests')
local inchannelpriority = lovr.thread
                              .getChannel('prioritychunkcolliderrequests')
local outchannel = lovr.thread.getChannel('chunkcolliderresults')

local collider2world = lovr.thread.getChannel('collider2world')
local world2collider = lovr.thread.getChannel('world2collider')

local function coord2index(i, j, k, xpc, ypc, zpc)
    local idx = (i - 1) * ypc * zpc + (j - 1) * zpc + k
    -- If block would be out-of-bounds, return -1 as index
    if i < 1 or j < 1 or k < 1 or i > xpc or j > ypc or k > zpc then idx = -1 end
    return idx
end
function index2coord(idx, xpc, ypc, zpc)
    local k = (idx - 1) % zpc + 1
    local j = ((idx - k) % (ypc * zpc)) / zpc + 1
    local i = (idx - k - (j - 1) * zpc) / (ypc * zpc) + 1
    return i, j, k
end

function naiveGreedyMesh(blockIDs, offset, bs, xpc, ypc, zpc)
    local positions = {}
    local scales = {}
    local minx, miny, minz, maxx, maxy, maxz = 0, 0, 0, 0, 0, 0
    for idx = 1, #blockIDs do
        if blockIDs[idx] > 0 then
            local b = blockIDs[idx]
            local x, y, z = index2coord(idx, xpc, ypc, zpc)
            minx, maxx, miny, maxy, minz, maxz = x, x, y, y, z, z
            -- Expand as far as possible along X axis
            while blockIDs[coord2index(x, y, z, xpc, ypc, zpc)] > 0 and x < xpc do
                maxx = x
                -- Mark as meshed
                blockIDs[coord2index(x, y, z, xpc, ypc, zpc)] = 0
                x = x + 1
            end
            -- Expand as far as possible along Y axis
            local expand = true
            while expand and y < ypc do
                y = y + 1
                for x = minx, maxx do
                    if blockIDs[coord2index(x, y, z, xpc, ypc, zpc)] == 0 then
                        expand = false
                    end
                end
                if expand then
                    maxy = y
                    -- Mark as meshed
                    for x = minx, maxx do
                        blockIDs[coord2index(x, y, z, xpc, ypc, zpc)] = 0
                    end
                end
            end
            -- Expand as far as possible along Z axis
            expand = true
            while expand and z < zpc do
                z = z + 1
                for x = minx, maxx do
                    for y = miny, maxy do
                        if blockIDs[coord2index(x, y, z, xpc, ypc, zpc)] == 0 then
                            expand = false
                        end
                    end
                end
                if expand then
                    maxz = z
                    -- Mark as meshed
                    for x = minx, maxx do
                        for y = miny, maxy do
                            blockIDs[coord2index(x, y, z, xpc, ypc, zpc)] = 0
                        end
                    end
                end
            end
            -- print(minx, maxx, miny, maxy, minz,maxz)
            local pos = {
                (minx + maxx) * bs / 2 + offset[1],
                (miny + maxy) * bs / 2 + offset[2],
                (minz + maxz) * bs / 2 + offset[3]
            }
            local scale = {
                (maxx - minx + 1) * bs, (maxy - miny + 1) * bs,
                (maxz - minz + 1) * bs
            }
            table.insert(positions, pos)
            table.insert(scales, scale)
        end
    end
    return positions, scales
end

function processcollider(input)
    input = serializer:deserialize(input)
    collider2world:push(serializer:serialize({
        fun = "getChunkBlocks",
        Cx = input.Cx,
        Cy = input.Cy,
        Cz = input.Cz,
        offset = input.offset,
        scale = input.scale,
        xpc = input.xpc,
        ypc = input.ypc,
        zpc = input.zpc
    }))
    -- print("Recieved mesh order", input.Cx, input.Cy, input.Cz)
end
local function processworld(input)
    local input = serializer:deserialize(input)
    local blocks = {}
    for i = 1, #input.blocks do
        for j = 1, #input.blocks[i] do
            for k = 1, #input.blocks[i][j] do
                blocks[#blocks + 1] = input.blocks[i][j][k]
            end
        end
    end
    -- print("Recieved world data", input.Cx, input.Cy, input.Cz,l)
    local positions, scales = naiveGreedyMesh(blocks, input.offset, input.scale,
                                              input.xpc, input.ypc, input.zpc)
    local dat = serializer:serialize({
        positions = positions,
        scales = scales,
        Cx = input.Cx,
        Cy = input.Cy,
        Cz = input.Cz
    })
    outchannel:push(dat)
end

while true do
    while true do
        input = inchannelpriority:pop()
        if input ~= nil then
            processgraphics(input)
        else
            break
        end
    end
    input = inchannel:pop()
    if input ~= nil then processcollider(input) end
    input = world2collider:pop()
    if input ~= nil then processworld(input) end
    lovr.timer.sleep(1 / 120)
end
