local lovr = {
    thread = require 'lovr.thread',
    math = require 'lovr.math',
    filesystem = require 'lovr.filesystem',
    timer = require 'lovr.timer'
}
local atlasconf = require "atlastexture"
local serializer = require("serialization")
local uvlookup, sidelookup, _, texlookup =
    atlasconf:make_atlas_decoder("derived/atlasdecoder.toml")

local inchannel = lovr.thread.getChannel('chunkmeshrequests')
local inchannelpriority = lovr.thread.getChannel('prioritychunkmeshrequests')
local outchannel = lovr.thread.getChannel('chunkmeshresults')

local mesher2world = lovr.thread.getChannel('mesher2world')
local world2mesher = lovr.thread.getChannel('world2mesher')

local function coord2index(i, j, k, xpc, ypc, zpc)
    -- If block would be out-of-bounds, return nil as index
    if i < 1 or j < 1 or k < 1 or i > (xpc + 2) or j > (ypc + 2) or k >
        (zpc + 2) then
        return nil
    else
        return (i - 1) * (ypc + 2) * (zpc + 2) + (j - 1) * (zpc + 2) + k
    end
end

local function make_mesh(blockIDs, blockLightsList, offset, maxblocks, scale,
                         xpc, ypc, zpc)
    local vertices = {}
    local indices = {{}, {}, {}, {}, {}, {}}
    for ix = 2, xpc + 1 do
        for iy = 2, ypc + 1 do
            for iz = 2, zpc + 1 do
                local idx = coord2index(ix, iy, iz, xpc, ypc, zpc)
                local blockID = blockIDs[idx]
                if ((blockID ~= nil) and (blockID > 0)) then
                    local topidx = coord2index(ix, iy + 1, iz, xpc, ypc, zpc)
                    local topface = (blockIDs[topidx] or 1) == 0
                    local bottomidx = coord2index(ix, iy - 1, iz, xpc, ypc, zpc)
                    local bottomface = (blockIDs[bottomidx] or 1) == 0
                    local westidx = coord2index(ix + 1, iy, iz, xpc, ypc, zpc)
                    local westface = (blockIDs[westidx] or 1) == 0
                    local eastidx = coord2index(ix - 1, iy, iz, xpc, ypc, zpc)
                    local eastface = (blockIDs[eastidx] or 1) == 0
                    local northidx = coord2index(ix, iy, iz - 1, xpc, ypc, zpc)
                    local northface = (blockIDs[northidx] or 1) == 0
                    local southidx = coord2index(ix, iy, iz + 1, xpc, ypc, zpc)
                    local southface = (blockIDs[southidx] or 1) == 0
                    -- Start on top faces
                    local side = sidelookup["top"]
                    if topface then
                        -- Fix this to be index of [blockID][side]
                        local texid = texlookup[side][blockID]
                        local uvx = uvlookup["x"][texid]
                        local uvy = uvlookup["y"][texid]
                        vertices[#vertices + 1] = {
                            offset[1] + ix * scale,
                            offset[2] + (iy + 1) * scale,
                            offset[3] + iz * scale, 0, 0, 1, uvx[1], uvy[1],
                            blockLightsList[topidx], blockLightsList[topidx],
                            blockLightsList[topidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + ix * scale,
                            offset[2] + (iy + 1) * scale,
                            offset[3] + (iz + 1) * scale, 0, 0, 1, uvx[1],
                            uvy[2], blockLightsList[topidx],
                            blockLightsList[topidx], blockLightsList[topidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + (ix + 1) * scale,
                            offset[2] + (iy + 1) * scale,
                            offset[3] + iz * scale, 0, 0, 1, uvx[2], uvy[1],
                            blockLightsList[topidx], blockLightsList[topidx],
                            blockLightsList[topidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + (ix + 1) * scale,
                            offset[2] + (iy + 1) * scale,
                            offset[3] + (iz + 1) * scale, 0, 0, 1, uvx[2],
                            uvy[2], blockLightsList[topidx],
                            blockLightsList[topidx], blockLightsList[topidx], 1
                        }
                        indices[side][#indices[side] + 1] = #vertices - 3
                        indices[side][#indices[side] + 1] = #vertices - 2
                        indices[side][#indices[side] + 1] = #vertices - 1
                        indices[side][#indices[side] + 1] = #vertices - 1
                        indices[side][#indices[side] + 1] = #vertices - 2
                        indices[side][#indices[side] + 1] = #vertices
                    end
                    -- Start on bottom faces
                    local side = sidelookup["bottom"]
                    if bottomface then
                        -- Fix this to be index of [blockID][side]
                        local texid = texlookup[side][blockID]
                        local uvx = uvlookup["x"][texid]
                        local uvy = uvlookup["y"][texid]
                        vertices[#vertices + 1] = {
                            offset[1] + ix * scale, offset[2] + iy * scale,
                            offset[3] + iz * scale, 0, 0, -1, uvx[1], uvy[1],
                            blockLightsList[bottomidx],
                            blockLightsList[bottomidx],
                            blockLightsList[bottomidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + (ix + 1) * scale,
                            offset[2] + iy * scale, offset[3] + iz * scale, 0,
                            0, -1, uvx[2], uvy[1], blockLightsList[bottomidx],
                            blockLightsList[bottomidx],
                            blockLightsList[bottomidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + ix * scale, offset[2] + iy * scale,
                            offset[3] + (iz + 1) * scale, 0, 0, -1, uvx[1],
                            uvy[2], blockLightsList[bottomidx],
                            blockLightsList[bottomidx],
                            blockLightsList[bottomidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + (ix + 1) * scale,
                            offset[2] + iy * scale,
                            offset[3] + (iz + 1) * scale, 0, 0, -1, uvx[2],
                            uvy[2], blockLightsList[bottomidx],
                            blockLightsList[bottomidx],
                            blockLightsList[bottomidx], 1
                        }
                        indices[side][#indices[side] + 1] = #vertices - 3
                        indices[side][#indices[side] + 1] = #vertices - 2
                        indices[side][#indices[side] + 1] = #vertices - 1
                        indices[side][#indices[side] + 1] = #vertices - 1
                        indices[side][#indices[side] + 1] = #vertices - 2
                        indices[side][#indices[side] + 1] = #vertices
                    end
                    -- Start on west faces
                    local side = sidelookup["west"]
                    if westface then
                        -- Fix this to be index of [blockID][side]
                        local texid = texlookup[side][blockID]
                        local uvx = uvlookup["x"][texid]
                        local uvy = uvlookup["y"][texid]
                        vertices[#vertices + 1] = {
                            offset[1] + (ix + 1) * scale,
                            offset[2] + iy * scale, offset[3] + iz * scale, 0,
                            0, 1, uvx[1], uvy[1], blockLightsList[westidx],
                            blockLightsList[westidx], blockLightsList[westidx],
                            1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + (ix + 1) * scale,
                            offset[2] + (iy + 1) * scale,
                            offset[3] + iz * scale, 0, 0, 1, uvx[2], uvy[1],
                            blockLightsList[westidx], blockLightsList[westidx],
                            blockLightsList[westidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + (ix + 1) * scale,
                            offset[2] + iy * scale,
                            offset[3] + (iz + 1) * scale, 0, 0, 1, uvx[1],
                            uvy[2], blockLightsList[westidx],
                            blockLightsList[westidx], blockLightsList[westidx],
                            1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + (ix + 1) * scale,
                            offset[2] + (iy + 1) * scale,
                            offset[3] + (iz + 1) * scale, 0, 0, 1, uvx[2],
                            uvy[2], blockLightsList[westidx],
                            blockLightsList[westidx], blockLightsList[westidx],
                            1
                        }
                        indices[side][#indices[side] + 1] = #vertices - 3
                        indices[side][#indices[side] + 1] = #vertices - 2
                        indices[side][#indices[side] + 1] = #vertices - 1
                        indices[side][#indices[side] + 1] = #vertices - 1
                        indices[side][#indices[side] + 1] = #vertices - 2
                        indices[side][#indices[side] + 1] = #vertices
                    end
                    -- Start on east faces
                    local side = sidelookup["east"]
                    if eastface then
                        -- Fix this to be index of [blockID][side]
                        local texid = texlookup[side][blockID]
                        local uvx = uvlookup["x"][texid]
                        local uvy = uvlookup["y"][texid]
                        vertices[#vertices + 1] = {
                            offset[1] + ix * scale, offset[2] + iy * scale,
                            offset[3] + iz * scale, 0, 0, -1, uvx[1], uvy[1],
                            blockLightsList[eastidx], blockLightsList[eastidx],
                            blockLightsList[eastidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + ix * scale, offset[2] + iy * scale,
                            offset[3] + (iz + 1) * scale, 0, 0, -1, uvx[1],
                            uvy[2], blockLightsList[eastidx],
                            blockLightsList[eastidx], blockLightsList[eastidx],
                            1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + ix * scale,
                            offset[2] + (iy + 1) * scale,
                            offset[3] + iz * scale, 0, 0, -1, uvx[2], uvy[1],
                            blockLightsList[eastidx], blockLightsList[eastidx],
                            blockLightsList[eastidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + ix * scale,
                            offset[2] + (iy + 1) * scale,
                            offset[3] + (iz + 1) * scale, 0, 0, -1, uvx[2],
                            uvy[2], blockLightsList[eastidx],
                            blockLightsList[eastidx], blockLightsList[eastidx],
                            1
                        }
                        indices[side][#indices[side] + 1] = #vertices - 3
                        indices[side][#indices[side] + 1] = #vertices - 2
                        indices[side][#indices[side] + 1] = #vertices - 1
                        indices[side][#indices[side] + 1] = #vertices - 1
                        indices[side][#indices[side] + 1] = #vertices - 2
                        indices[side][#indices[side] + 1] = #vertices
                    end
                    -- Start on north faces
                    local side = sidelookup["north"]
                    if northface then
                        -- Fix this to be index of [blockID][side]
                        local texid = texlookup[side][blockID]
                        local uvx = uvlookup["x"][texid]
                        local uvy = uvlookup["y"][texid]
                        vertices[#vertices + 1] = {
                            offset[1] + ix * scale, offset[2] + iy * scale,
                            offset[3] + iz * scale, 0, 0, -1, uvx[1], uvy[1],
                            blockLightsList[northidx],
                            blockLightsList[northidx],
                            blockLightsList[northidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + ix * scale,
                            offset[2] + (iy + 1) * scale,
                            offset[3] + iz * scale, 0, 0, -1, uvx[2], uvy[1],
                            blockLightsList[northidx],
                            blockLightsList[northidx],
                            blockLightsList[northidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + (ix + 1) * scale,
                            offset[2] + iy * scale, offset[3] + iz * scale, 0,
                            0, -1, uvx[1], uvy[2], blockLightsList[northidx],
                            blockLightsList[northidx],
                            blockLightsList[northidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + (ix + 1) * scale,
                            offset[2] + (iy + 1) * scale,
                            offset[3] + iz * scale, 0, 0, -1, uvx[2], uvy[2],
                            blockLightsList[northidx],
                            blockLightsList[northidx],
                            blockLightsList[northidx], 1
                        }
                        indices[side][#indices[side] + 1] = #vertices - 3
                        indices[side][#indices[side] + 1] = #vertices - 2
                        indices[side][#indices[side] + 1] = #vertices - 1
                        indices[side][#indices[side] + 1] = #vertices - 1
                        indices[side][#indices[side] + 1] = #vertices - 2
                        indices[side][#indices[side] + 1] = #vertices
                    end
                    -- Start on south faces
                    local side = sidelookup["south"]
                    if southface then
                        -- Fix this to be index of [blockID][side]
                        local texid = texlookup[side][blockID]
                        local uvx = uvlookup["x"][texid]
                        local uvy = uvlookup["y"][texid]
                        vertices[#vertices + 1] = {
                            offset[1] + ix * scale, offset[2] + iy * scale,
                            offset[3] + (iz + 1) * scale, 0, 0, 1, uvx[1],
                            uvy[1], blockLightsList[southidx],
                            blockLightsList[southidx],
                            blockLightsList[southidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + (ix + 1) * scale,
                            offset[2] + iy * scale,
                            offset[3] + (iz + 1) * scale, 0, 0, 1, uvx[1],
                            uvy[2], blockLightsList[southidx],
                            blockLightsList[southidx],
                            blockLightsList[southidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + ix * scale,
                            offset[2] + (iy + 1) * scale,
                            offset[3] + (iz + 1) * scale, 0, 0, 1, uvx[2],
                            uvy[1], blockLightsList[southidx],
                            blockLightsList[southidx],
                            blockLightsList[southidx], 1
                        }
                        vertices[#vertices + 1] = {
                            offset[1] + (ix + 1) * scale,
                            offset[2] + (iy + 1) * scale,
                            offset[3] + (iz + 1) * scale, 0, 0, 1, uvx[2],
                            uvy[2], blockLightsList[southidx],
                            blockLightsList[southidx],
                            blockLightsList[southidx], 1
                        }
                        indices[side][#indices[side] + 1] = #vertices - 3
                        indices[side][#indices[side] + 1] = #vertices - 2
                        indices[side][#indices[side] + 1] = #vertices - 1
                        indices[side][#indices[side] + 1] = #vertices - 1
                        indices[side][#indices[side] + 1] = #vertices - 2
                        indices[side][#indices[side] + 1] = #vertices
                    end
                end
            end
        end
    end
    -- print("Made mesh:", #vertices)
    return indices, vertices
end

local function getdata(blockIDs, blockLights, xpc, ypc, zpc)
    local blockIDsList = {}
    local isempty = true
    for i = 1, xpc + 2 do
        for j = 1, ypc + 2 do
            for k = 1, zpc + 2 do
                local b = blockIDs[i][j][k]
                if isempty and b > 0 and
                    (1 < i and 1 < j and 1 < k and i < xpc + 2 and j < ypc + 2 and
                        k < zpc + 2) then isempty = false end
                blockIDsList[#blockIDsList + 1] = b
            end
        end
    end
    if isempty then return nil, nil end
    local blockLightsList = {}
    if not isempty then
        local isempty = true
        for i = 1, xpc + 2 do
            for j = 1, ypc + 2 do
                for k = 1, zpc + 2 do
                    local b = blockLights[i][j][k]
                    blockLightsList[#blockLightsList + 1] = b
                end
            end
        end
    end
    return blockIDsList, blockLightsList
end

local function processgraphics(input)
    input = serializer:deserialize(input)
    mesher2world:push(serializer:serialize({
        fun = "getChunkBlocksPadded",
        Cx = input.Cx,
        Cy = input.Cy,
        Cz = input.Cz,
        offset = input.offset,
        scale = input.scale,
        xpc = input.xpc,
        ypc = input.ypc,
        zpc = input.zpc
    }))
    mesher2world:push(serializer:serialize({
        fun = "getChunkLightsPadded",
        Cx = input.Cx,
        Cy = input.Cy,
        Cz = input.Cz,
        offset = input.offset,
        scale = input.scale,
        xpc = input.xpc,
        ypc = input.ypc,
        zpc = input.zpc
    }))
    -- print("Recieved mesh order", input.Cx, input.Cy, input.Cz)
end
local blockLights, blockIDs
local function processworld(input)
    local input = serializer:deserialize(input)
    -- print("Recieved world data", input.Cx, input.Cy, input.Cz,l)
    if input.fun == "getChunkBlocksPadded" then
        blockIDs = input.blocks
    elseif input.fun == "getChunkLightsPadded" then
        blockLights = input.light
    else
        error("Bad results function")
    end
    if blockLights and blockIDs then
        blockIDsList, blockLightsList = getdata(blockIDs, blockLights,
                                                input.xpc, input.ypc, input.zpc)
        blockLights = nil
        blockIDs = nil
        local indices, vertices
        if blockIDsList and blockLightsList then
            input.offset = lovr.math.newVec3(input.offset[1], input.offset[2],
                                             input.offset[3])
            indices, vertices = make_mesh(blockIDsList, blockLightsList,
                                          input.offset, 4, input.scale,
                                          input.xpc, input.ypc, input.zpc)
        else
            indices, vertices = nil, nil
        end
        local dat = serializer:serialize({
            vertices = vertices,
            indices = indices,
            Cx = input.Cx,
            Cy = input.Cy,
            Cz = input.Cz
        })
        outchannel:push(dat)
        l = 0
        if indices then for i = 1, 6 do l = l + #indices[i] end end
        if vertices then l = l + #vertices end
        -- print("Pushed mesh", input.Cx, input.Cy, input.Cz, l)
    end
end
while true do
    while true do
        input = inchannelpriority:pop()
        if input ~= nil then
            processgraphics(input)
        else
            break
        end
    end
    input = inchannel:pop()
    if input ~= nil then processgraphics(input) end
    input = world2mesher:pop()
    if input ~= nil then processworld(input) end
    lovr.timer.sleep(1 / 120)
end
