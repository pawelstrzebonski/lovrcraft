# Dependencies

The running of this application requires the following external code:

* [`LÖVR`](https://github.com/bjornbytes/lovr): The VR framework used to run this project
* [`binser`](https://github.com/bakpakin/binser): Lua serialization library (1 of many options)
* [`bitser`](https://github.com/gvx/bitser): Lua serialization library (1 of many options)
* [`lua-toml`](https://github.com/jonstoler/lua-toml/): Lua TOML parsing library
* [`perlin`](https://stackoverflow.com/a/33425812): Lua Perlin noise script
* [`ser`](https://github.com/gvx/ser): Lua serialization library (1 of many options)
* [`serpent`](https://github.com/pkulchenko/serpent): Lua serialization library (1 of many options)
* [`Smallfolk`](https://github.com/gvx/Smallfolk): Lua serialization library (1 of many options)

In particular, the varous Lua serialization libraries are all included, however only a single one will be configured to run. These should be down-selected to the most performant and/or compatible library.

Various external assets are also used in the project:

* [`minetest-game-mods/default`](https://github.com/minetest-game-mods/default): Source of block textures
* [`clouds-skybox-1`](https://opengameart.org/content/clouds-skybox-1): Source of skybox textures

The external assets may be processed by scripts into a different form that is compatible with this application. These scripts may depend of additional programs and libraries, including:

* [`convert`/`mogrify`](https://imagemagick.org/): Parts of the ImageMagick toolset for image processing/manipulation
* [`python`](https://www.python.org/): Programming language used for some scripts
* [`toml`](https://github.com/uiri/toml): Python package for importing/exporting TOML files
