# TODOs

This project is by no means finished/polished. A the following is a shopping list of improvements (in no particular order):

* Player/controller physics and motion improvements
* Graphics capability improvements
	* transparency in blocks
	* lighting/shading
	* multiple atlas texture options (e.g. multiple resolutions) and automatic selection based on GPU capabilities (ensure loaded texture is never greater than GPU limit)
	* etc
* Performance improvements
	* faster inter-thread communications/(de)serialization (create model/mesh-data off main thread and pass that instead of serializing tables)
	* optimized meshing and UV mapping to allow greedy meshing with atlas textures(consult [ref](https://0fps.net/2012/06/30/meshing-in-a-minecraft-game/)
	* proper fulstrum culling
	* chunk size tuning/optimization
	* collider geometry and physics engine optimization
	* faster meshing
	* better datatypes for block data?
	* etc
* Content improvements
	* more blocks
	* better world generation
	* etc
* Core improvements
	* support loading worlds to/from save files
	* improved interaction with world
	* crafting and block inventory
	* support both single- and multi-thread operation for compatibility/performance compromise (thread should work on web, need to verify)
* General code/project improvements
	* tests
	* CI/CD
	* documentation
	* etc
