local ChunkColliderManager = {}
ChunkColliderManager.chunkDict = {}
ChunkColliderManager.renderList = {}
ChunkColliderManager.updateList = {}
ChunkColliderManager.loadRange = 1
ChunkColliderManager.unloadRange = 2

local serializer = require("serialization")
require "coord_conversions"

local VoxelConf

ChunkColliderManager.WorldPhysics = lovr.physics.newWorld()
ChunkColliderManager.WorldPhysics:setSleepingAllowed(true)
local inchannel = lovr.thread.getChannel("chunkcolliderresults")
local outchannel = lovr.thread.getChannel("chunkcolliderrequests")
local outchannelpriority = lovr.thread.getChannel("chunkcolliderrequests")

function ChunkColliderManager:setup(vc)
    ChunkColliderManager:update_range(2)
    VoxelConf = vc
end

function ChunkColliderManager:newChunk(Cx, Cy, Cz)
    local self = {}
    self.center = lovr.math.newVec3(ChunkToGlobal(Cx, Cy, Cz,
                                                  VoxelConf.XperChunk / 2,
                                                  VoxelConf.YperChunk / 2,
                                                  VoxelConf.ZperChunk / 2,
                                                  VoxelConf.BlockSize,
                                                  VoxelConf.XperChunk,
                                                  VoxelConf.YperChunk,
                                                  VoxelConf.ZperChunk))
    local offset = lovr.math.newVec3(ChunkToGlobal(Cx, Cy, Cz, 0, 0, 0,
                                                   VoxelConf.BlockSize,
                                                   VoxelConf.XperChunk,
                                                   VoxelConf.YperChunk,
                                                   VoxelConf.ZperChunk))
    self.isempty = false
    -- Add a default collider
    self.colliders = {}
    self.colliders[1] = ChunkColliderManager.WorldPhysics:newBoxCollider(
                            self.center[1], self.center[2], self.center[3],
                            VoxelConf.XperChunk * VoxelConf.BlockSize,
                            VoxelConf.YperChunk * VoxelConf.BlockSize,
                            VoxelConf.ZperChunk * VoxelConf.BlockSize)
    self.colliders[1]:setKinematic(true)
    -- Send request for proper colliders
    local reqdata = {
        offset = lovr.math.newVec3(offset),
        Cx = Cx,
        Cy = Cy,
        Cz = Cz,
        scale = VoxelConf.BlockSize,
        xpc = VoxelConf.XperChunk,
        ypc = VoxelConf.YperChunk,
        zpc = VoxelConf.ZperChunk
    }
    reqdata.offset = {reqdata.offset[1], reqdata.offset[2], reqdata.offset[3]}
    local req = serializer:serialize(reqdata)
    if isupdate then
        outchannelpriority:push(req)
    else
        outchannel:push(req)
    end
    if ChunkColliderManager.chunkDict[Cx] == nil then
        ChunkColliderManager.chunkDict[Cx] = {}
    end
    if ChunkColliderManager.chunkDict[Cx][Cy] == nil then
        ChunkColliderManager.chunkDict[Cx][Cy] = {}
    end
    ChunkColliderManager.chunkDict[Cx][Cy][Cz] = self
end

function ChunkColliderManager:destroyColliders(Cx, Cy, Cz)
    local self = ChunkColliderManager.chunkDict[Cx][Cy][Cz]
    if self.colliders ~= nil and #self.colliders > 0 then
        local nc = #self.colliders
        for i = 1, nc do
            self.colliders[i]:destroy()
            self.colliders[i] = nil
        end
    end
end

function ChunkColliderManager:updateChunk(WorldData, Cx, Cy, Cz)
    if ChunkColliderManager.chunkDict[Cx] ~= nil and
        ChunkColliderManager.chunkDict[Cx][Cy] ~= nil and
        ChunkColliderManager.chunkDict[Cx][Cy][Cz] ~= nil then
        -- Destroy the existing colliders...
        ChunkColliderManager:destroyColliders(Cx, Cy, Cz)
        ChunkColliderManager.chunkDict[Cx][Cy][Cz] = nil
    end
    -- And recreate chunk from scratch
    ChunkColliderManager:newChunk(WorldData, Cx, Cy, Cz)
end

function ChunkColliderManager:addColliderToChunk(msg)
    -- Deserialize message
    local resp = serializer:deserialize(msg)
    -- Get chunk coordiantes
    local Cx, Cy, Cz = resp.Cx, resp.Cy, resp.Cz
    if ChunkColliderManager.chunkDict[Cx] == nil or
        ChunkColliderManager.chunkDict[Cx][Cy] == nil or
        ChunkColliderManager.chunkDict[Cx][Cy][Cz] == nil then
        print("NOTICE: Adding collider to nil chunk")
        return
    end
    -- Find said chunk
    local self = ChunkColliderManager.chunkDict[Cx][Cy][Cz]
    -- Clear out existing colliders
    ChunkColliderManager:destroyColliders(Cx, Cy, Cz)
    -- And add in the new colliders
    for i = 1, #resp.positions do
        local pos = resp.positions[i]
        local scal = resp.scales[i]
        self.colliders[i] = ChunkColliderManager.WorldPhysics:newBoxCollider(
                                pos[1], pos[2], pos[3], scal[1], scal[2],
                                scal[3])
        self.colliders[i]:setKinematic(true)
    end
end

function ChunkColliderManager:update(dt, Px, Py, Pz, TimeManager)
    -- Update collisions/physics
    ChunkColliderManager.WorldPhysics:update(dt)

    local abs = math.abs
    -- Remove out-of-range chunks
    for i, idx in ipairs(ChunkColliderManager.renderList) do
        -- Get coordinates of chunk-under-test
        local Cx, Cy, Cz = idx[1], idx[2], idx[3]
        -- Test if outside unload range
        if abs(Cx - Px) > ChunkColliderManager.unloadRange or abs(Cy - Py) >
            ChunkColliderManager.unloadRange or abs(Cz - Pz) >
            ChunkColliderManager.unloadRange then
            -- Destroy all colliders
            ChunkColliderManager:destroyColliders(Cx, Cy, Cz)
            -- Remove object from dictionary
            ChunkColliderManager.chunkDict[Cx][Cy][Cz] = nil
            -- Remove from list of chunks to render
            table.remove(ChunkColliderManager.renderList, i)
        end
    end
    -- Load chunks within render range
    for x, coord in ipairs(ChunkColliderManager.updateList) do
        -- Get relative coordinates
        local i, j, k = coord[1], coord[2], coord[3]
        -- Limit to world height limits
        if (Py + j >= 1) and (Py + j <= VoxelConf.ChunksHeight) then
            -- Calculate coordinates of chunk-under-test
            local Cx, Cy, Cz = Px + i, Py + j, Pz + k
            -- Check if chunk within list of to-render chunks
            if not containsTable(self.renderList, {Cx, Cy, Cz}) then
                -- If not, create the chunk and add to list
                TimeManager:startTime()
                ChunkColliderManager:newChunk(Cx, Cy, Cz)
                ChunkColliderManager.renderList[#ChunkColliderManager.renderList +
                    1] = {Cx, Cy, Cz}
                TimeManager:stopColliderQueuingTime()
            end
            if not TimeManager:shouldIQueueCollider() then break end
        end
    end
    -- Check the inbox of collider meshing results, and, time permitting, add them to the corresponding chunk
    local msg = inchannel:pop()
    while msg ~= nil do
        TimeManager:startTime()
        ChunkColliderManager:addColliderToChunk(msg)
        TimeManager:stopCollidingTime()
        if not TimeManager:shouldICollide() then break end
        msg = inchannel:pop()
    end
end

function ChunkColliderManager:update_range(lr)
    local updatelist = {}
    -- Create a list of all offsets within provided distance...
    for i = -lr, lr do
        for j = -lr, lr do
            for k = -lr, lr do
                if i * i + j * j + k * k <= lr * lr then
                    updatelist[#updatelist + 1] = {i, j, k}
                end
            end
        end
    end
    -- Distance comparison function
    local cmp = function(a, b)
        local asum = (math.abs(a[1]) + math.abs(a[2]) + math.abs(a[3]))
        local bsum = (math.abs(b[1]) + math.abs(b[2]) + math.abs(b[3]))
        if asum < bsum then
            return true
        elseif asum > bsum then
            return false
        else
            return false
        end
    end
    -- And sort the list of offsets from nearest to farthest
    table.sort(updatelist, cmp)
    ChunkColliderManager.updateList = updatelist
end

-- Round a point to the nearest block center
local function round2blocksize(v)
    local BlockSize = VoxelConf.BlockSize
    local r = v / BlockSize + 0.5
    return vec3(BlockSize * math.floor(r[1]), BlockSize * math.floor(r[2]),
                BlockSize * math.floor(r[3]))
end

function ChunkColliderManager:raycast(pos, vec)
    local intersections = {}
    -- Add all intersections to a list
    local callback = function(shape, x, y, z, nx, ny, nz)
        intersections[#intersections + 1] = {x, y, z, nx, ny, nz}
    end
    ChunkColliderManager.WorldPhysics:raycast(pos, pos + vec, callback)
    local bestintersection
    -- If no intersections found...
    if #intersections == 0 then
        return nil, nil
        -- If only a single intersection...
    elseif #intersections == 1 then
        bestintersection = intersections[1]
        -- Else find the first/nearest intersection to user
    else
        bestintersection = intersections[1]
        local bestdist = pos:distance(vec3(bestintersection[1],
                                           bestintersection[2],
                                           bestintersection[3]))
        for i = 2, #intersections do
            local dist = pos:distance(vec3(intersections[i][1],
                                           intersections[i][2],
                                           intersections[i][3]))
            if dist < bestdist then
                bestintersection = intersections[i]
                bestdist = dist
            end
        end
    end
    -- Calculate center coordinates of the block being pointed to...
    local pointedblock = round2blocksize(
                             vec3(bestintersection[1], bestintersection[2],
                                  bestintersection[3]) -
                                 vec3(bestintersection[4], bestintersection[5],
                                      bestintersection[6]) * VoxelConf.BlockSize /
                                 2)
    -- Or the adjacent space being pointed to
    local pointedspace = round2blocksize(
                             vec3(bestintersection[1], bestintersection[2],
                                  bestintersection[3]) +
                                 vec3(bestintersection[4], bestintersection[5],
                                      bestintersection[6]) * VoxelConf.BlockSize /
                                 2)
    -- Convert real-world coordinates into block data coordinates
    pointedblock = vec3(GlobalToBlock(pointedblock[1], pointedblock[2],
                                      pointedblock[3], VoxelConf.BlockSize))
    pointedspace = vec3(GlobalToBlock(pointedspace[1], pointedspace[2],
                                      pointedspace[3], VoxelConf.BlockSize))
    return pointedblock, pointedspace
end

return ChunkColliderManager
